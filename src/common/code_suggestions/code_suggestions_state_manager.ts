import * as vscode from 'vscode';
import {
  AI_ASSISTED_CODE_SUGGESTIONS_MODE,
  getAiAssistedCodeSuggestionsConfiguration,
} from '../utils/extension_configuration';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from './constants';
import { GitLabPlatformManager } from '../platform/gitlab_platform';
import { GitLabPlatformManagerForCodeSuggestions } from './gitlab_platform_manager_for_code_suggestions';
import { diffEmitter } from './diff_emitter';

type ValueOf<T> = T[keyof T];

export type VisibleCodeSuggestionsState = ValueOf<typeof VisibleCodeSuggestionsState>;
export const VisibleCodeSuggestionsState = {
  DISABLED_VIA_SETTINGS: 'code-suggestions-global-disabled-via-settings',
  READY: 'code-suggestions-global-ready',
  DISABLED_BY_USER: 'code-suggestions-disabled-by-user',
  NO_ACCOUNT: 'code-suggestions-no-account',
  UNSUPPORTED_LANGUAGE: 'code-suggestions-document-unsupported-language',
  ERROR: 'code-suggestions-error',
  LOADING: 'code-suggestions-loading',
} as const;

const isSupportedLanguage = (editor: vscode.TextEditor) =>
  AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES.includes(editor.document.languageId);

export class CodeSuggestionsStateManager {
  // //////////
  // boolean flags that indicate code suggestions are not on
  #disabledInSettings: boolean;

  #isDisabledByUserForSession = false;

  #isMissingAccount = false;

  #isUnsupportedLanguage = false;
  // //////////

  // //////////
  // boolean flags and counters indicating temporary states
  #isInErrorState = false;

  #loadingResources = new WeakSet();

  // this can't be a boolean flag because it's possible that response from first
  // request comes after we send second request (which would incorrectly set loading to false)
  #loadingCounter = 0;
  // //////////

  #subscriptions: vscode.Disposable[] = [];

  #gitlabPlatformManager: GitLabPlatformManager;

  #changeVisibleStateEmitter = diffEmitter(new vscode.EventEmitter<VisibleCodeSuggestionsState>());

  onDidChangeVisibleState = this.#changeVisibleStateEmitter.event;

  #changeDisabledByUserStateEmitter = diffEmitter(new vscode.EventEmitter<boolean>());

  onDidChangeDisabledByUserState = this.#changeDisabledByUserStateEmitter.event;

  constructor(gitlabPlatformManager: GitLabPlatformManager) {
    this.#gitlabPlatformManager = gitlabPlatformManager;
    this.#disabledInSettings = !getAiAssistedCodeSuggestionsConfiguration().enabled;
    const checkIfLanguageSupported = (te?: vscode.TextEditor) => {
      this.#isUnsupportedLanguage = te ? !isSupportedLanguage(te) : false;
      this.#fireChange();
    };
    checkIfLanguageSupported(vscode.window.activeTextEditor);

    this.#subscriptions.push(
      vscode.workspace.onDidChangeConfiguration(e => {
        if (e.affectsConfiguration(AI_ASSISTED_CODE_SUGGESTIONS_MODE)) {
          this.#disabledInSettings = !getAiAssistedCodeSuggestionsConfiguration().enabled;
          this.#fireChange();
        }
      }),
      vscode.window.onDidChangeActiveTextEditor(checkIfLanguageSupported),
    );
  }

  async init() {
    const checkIfAccountPresent = async () => {
      const manager = new GitLabPlatformManagerForCodeSuggestions(this.#gitlabPlatformManager);
      const platform = await manager.getGitLabPlatform();
      this.#isMissingAccount = !platform;
      this.#fireChange();
    };
    await checkIfAccountPresent();

    this.#gitlabPlatformManager.onAccountChange(checkIfAccountPresent);
  }

  isDisabledByUser() {
    return this.#disabledInSettings || this.#isDisabledByUserForSession;
  }

  /** isActive indicates whether the suggestions are on and suggestion requests are being sent to the API */
  isActive() {
    return !(
      this.#disabledInSettings ||
      this.#isDisabledByUserForSession ||
      this.#isMissingAccount ||
      this.#isUnsupportedLanguage
    );
  }

  getVisibleState(): VisibleCodeSuggestionsState {
    if (this.#disabledInSettings) {
      return VisibleCodeSuggestionsState.DISABLED_VIA_SETTINGS;
    }

    if (this.#isDisabledByUserForSession) {
      return VisibleCodeSuggestionsState.DISABLED_BY_USER;
    }

    if (this.#isMissingAccount) {
      return VisibleCodeSuggestionsState.NO_ACCOUNT;
    }

    if (this.#isUnsupportedLanguage) {
      return VisibleCodeSuggestionsState.UNSUPPORTED_LANGUAGE;
    }

    if (this.#isInErrorState) {
      return VisibleCodeSuggestionsState.ERROR;
    }

    if (this.#loadingCounter !== 0) {
      return VisibleCodeSuggestionsState.LOADING;
    }

    return VisibleCodeSuggestionsState.READY;
  }

  setLoadingResource(resource: object, isLoading: boolean) {
    const isResourceLoading = this.#loadingResources.has(resource);

    if (isResourceLoading !== isLoading) {
      this.setLoading(isLoading);
    }

    if (isLoading) {
      this.#loadingResources.add(resource);
    } else {
      this.#loadingResources.delete(resource);
    }
  }

  #fireChange = () => {
    this.#changeVisibleStateEmitter.fire(this.getVisibleState());
    this.#changeDisabledByUserStateEmitter.fire(this.isDisabledByUser());
  };

  setError = (isError: boolean) => {
    this.#isInErrorState = isError;
    this.#fireChange();
  };

  setLoading = (isLoading: boolean) => {
    if (isLoading) {
      this.#loadingCounter += 1;
    } else {
      this.#loadingCounter = Math.max(0, this.#loadingCounter - 1);
    }
    this.#fireChange();
  };

  setTemporaryDisabled = (isDisabled: boolean) => {
    this.#isDisabledByUserForSession = isDisabled;
    this.#fireChange();
  };

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
