import vscode from 'vscode';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { EventEmitter, diffEmitter } from './diff_emitter';

describe('diffEmitter', () => {
  let fakeEmitter: EventEmitter<string>;
  let testDiffEmitter: EventEmitter<string>;
  beforeEach(() => {
    fakeEmitter = {
      event: createFakePartial<vscode.Event<string>>({}),
      fire: jest.fn(),
      dispose: jest.fn(),
    };

    testDiffEmitter = diffEmitter(fakeEmitter);
  });

  it('exposes event and dispose from the original emitter', () => {
    expect(testDiffEmitter.event).toBe(fakeEmitter.event);
    expect(testDiffEmitter.dispose).toBe(fakeEmitter.dispose);
  });

  it('fires when the data is different', () => {
    testDiffEmitter.fire('a');

    expect(fakeEmitter.fire).toHaveBeenCalledWith('a');
    jest.mocked(fakeEmitter.fire).mockClear();

    testDiffEmitter.fire('b');

    expect(fakeEmitter.fire).toHaveBeenCalledWith('b');
  });

  it('does not fire if the data is the same', () => {
    testDiffEmitter.fire('a');

    expect(fakeEmitter.fire).toHaveBeenCalledWith('a');
    jest.mocked(fakeEmitter.fire).mockClear();

    testDiffEmitter.fire('a');

    expect(fakeEmitter.fire).not.toHaveBeenCalled();
  });
});
