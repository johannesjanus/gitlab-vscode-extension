import vscode from 'vscode';
import { isEqual } from 'lodash';

/** This interface exactly copies the vscode.EventEmitter class */
export interface EventEmitter<T> {
  event: vscode.Event<T>;
  fire(data: T): void;
  dispose(): void;
}

/** promotes EventEmitter into an emitter that will only fire change event if the value T has changed */
export const diffEmitter = <T>(emitter: EventEmitter<T>): EventEmitter<T> => {
  let previousValue: T;
  return {
    event: emitter.event,
    fire: (data: T) => {
      if (!isEqual(previousValue, data)) {
        previousValue = data;
        emitter.fire(data);
      }
    },
    dispose: emitter.dispose,
  };
};
