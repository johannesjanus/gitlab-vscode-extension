import { nextTick } from 'vue';
import { shallowMount } from '@vue/test-utils';
import { GlDuoChat } from '@gitlab/ui';
import App from './App.vue';

describe('Duo Chat Vue app', () => {
  let wrapper;
  let vsCodeApi;

  const findDuoChat = () => wrapper.findComponent(GlDuoChat);

  const createComponent = () => {
    wrapper = shallowMount(App);
  };

  beforeEach(() => {
    vsCodeApi = acquireVsCodeApi();
  });

  describe('application bootstrapping', () => {
    it('posts the `appReady` message when created', () => {
      expect(vsCodeApi.postMessage).not.toHaveBeenCalled();
      createComponent();
      expect(vsCodeApi.postMessage).toHaveBeenCalledWith({
        command: 'appReady',
      });
    });
  });

  describe('GlDuoChat integration', () => {
    it('renders the Duo Chat component', () => {
      createComponent();
      expect(findDuoChat().exists()).toBe(true);
    });

    it('correctly sets the props on Duo Chat', async () => {
      const chatMessages = [
        {
          content: 'Foo',
          role: 'user',
        },
        {
          content: 'Bar',
          role: 'assistant',
        },
      ];

      createComponent();
      expect(findDuoChat().props('messages')).toEqual([]);

      chatMessages.forEach(record => {
        window.dispatchEvent(
          new MessageEvent('message', {
            data: {
              eventType: 'newRecord',
              record,
            },
          }),
        );
      });
      await nextTick();

      expect(findDuoChat().props('messages')).toEqual(chatMessages);
    });
  });
});
